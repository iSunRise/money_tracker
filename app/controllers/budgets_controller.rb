class BudgetsController < ResourcesController
  def index
    budgets = Budget.includes(:monthly_budgets).order(id: :desc).map do |budget|
      budget.month = params[:month] || Date.current.month
      budget.year = params[:year] || Date.current.year
      budget
    end
    render json: budgets
  end


  def update
    budget = Budget.find(params[:id])
    budget.month = params[:month] || Date.current.month
    budget.year =  params[:year] || Date.current.year

    if budget.update(resource_params)
     render json: budget
    else
      render json: { errors: budget.errors.full_messages }, status: 422
    end
  end


  private

  def resource_class
    Budget
  end

  def resource_params
    params.require(:budget).permit(:name, :icon, :amount, :month, :year,
                                   :show_on_dashboard, :currency_symbol,
                                   :include_in_total)
  end
end
