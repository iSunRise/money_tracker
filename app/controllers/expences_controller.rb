class ExpencesController < ResourcesController
  def index
    from_date = params[:from] ? Date.parse(params[:from]) : Date.current.beginning_of_month
    to_date =   params[:to]   ? Date.parse(params[:to])   : Date.current.end_of_month
    expences = Expence.includes(:category, :budget)
                      .where(date: (from_date..to_date))
                      .order(id: :asc)
    if params[:budgetId].present? && params[:budgetId] != 'all'
      expences = expences.where(budget_id: params[:budgetId])
    end
    render json: expences
  end

  private

  def resource_class
    Expence
  end

  def resource_params
    params.require(:expence).permit(:name, :amount, :date, :description,
                                    :category_id, :budget_id,
                                    :currency_symbol, :rate)
  end
end
