class CategoriesController < ResourcesController
  def index
    render json: Category.includes(:budget).order(id: :desc)
  end

  private

  def resource_class
    Category
  end

  def resource_params
    params.require(:category).permit(:name, :icon, :budget_id)
  end
end
