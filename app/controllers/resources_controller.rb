class ResourcesController < ApplicationController
  def create
    resource = resource_class.new(resource_params)
    resource.account_id = current_user.account_id
    if resource.save
      render json: resource
    else
      render json: { errors: resource.errors.full_messages }, status: 422
    end
  end

  def update
    resource = resource_class.find(params[:id])
    if resource.update(resource_params)
      render json: resource
    else
      render json: { errors: resource.errors.full_messages }, status: 422
    end
  end

  def destroy
    resource_class.find(params[:id]).destroy
    head :ok
  end
end
