class StatisticsController < ResourcesController
  def show
    date = params[:date].present? ? Date.parse(params[:date]) : Date.current
    expences = Expence.includes(:budget, :category)
                      .where(date: [date.beginning_of_month..date.end_of_month])
                      .order(:budget_id)
    render json: [
      by_budgets_and_categories(expences)
    ]
  end

  private

  def by_budgets_and_categories(expences)
    budgets = []
    categories = []
    expences.group_by(&:budget).each do |budget, expences|
      bud = {
        name: budget.name,
        value: expences.map(&:amount).sum.to_f.round(2)
      }
      budgets << bud if bud[:value] > 0
      expences.group_by(&:category).each do |category, expences|
        cat = {
          name: category.try(:name) || 'Інше',
          value: expences.map(&:amount).sum.to_f.round(2)
        }
        categories << cat if cat[:value] > 0
      end
    end
    { budgets: budgets, categories: categories }
  end
end
