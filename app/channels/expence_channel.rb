class ExpenceChannel < ApplicationCable::Channel
  def subscribed
    stream_from "Account##{current_user.account_id}/ExpenceChannel"
  end
end