class BudgetChannel < ApplicationCable::Channel
  def subscribed
    stream_from "Account##{current_user.account_id}/BudgetChannel"
  end
end