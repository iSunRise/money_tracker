class CategoryChannel < ApplicationCable::Channel
  def subscribed
    stream_from "Account##{current_user.account_id}/CategoryChannel"
  end
end