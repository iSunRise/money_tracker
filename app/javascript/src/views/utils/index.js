import styled from 'styled-components'

export const White = styled.div`
  background: #fff;
`
export const PullRight = styled.span`
  float: right;
`

export const TextCenter = styled.span`
  text-align: center;
`
