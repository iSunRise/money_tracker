import React from 'react'
import { observer, inject } from 'mobx-react'
import { Row, Col, Spin } from 'antd'
import ReactEcharts from 'echarts-for-react'
import MonthsMenu from '../components/MonthsMenu'
import echarts from 'echarts'
import 'echarts/theme/dark'

@inject('statistic')
@observer
class Statistics extends React.Component {
  componentDidMount(){
    this.props.statistic.load()
  }
  generateColor(str) {
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
      hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    var colour = '#';
    for (var i = 0; i < 3; i++) {
      var value = (hash >> (i * 8)) & 0xFF;
      colour += ('00' + value.toString(16)).substr(-2);
    }
    return colour;
  }

  dateChanged(month, year){
    this.props.statistic.load({date: `01.${month}.${year}`})
  }

  render(){
    const { data, status } = this.props.statistic
    const stats = data.toJS()

    if (!stats.length){
      return(
        <Spin spinning={status.isLoading}>
          Loading...
        </Spin>
      )
    }

    const { budgets, categories } = stats[0]

    var option = {
      title: {
          text: 'Витрати'
      },
      tooltip: {
        show: true
      },
      series: [{
        name: 'Категорія',
        type: 'pie',
        radius: ['40%', '70%'],
        data: categories
      },{
        name: 'Бюджет',
        type: 'pie',
        radius: ['20%', '40%'],
        data: budgets,
        label: {
          normal: { position: 'inside' }
        }
      }]
    };

    
    return(
      <div>
        <Row>
          <Col>
            <MonthsMenu onDateChange={this.dateChanged.bind(this)}/>
          </Col>
        </Row>
        <Row>
          <Col>
          <ReactEcharts
            notMerge={true}
            lazyUpdate={true}
            theme="dark"
            option={option}
            style={{height: '600px', width: '100%'}} 
            />
          </Col>
        </Row>
      </div>
    )
  }
}

export default Statistics
