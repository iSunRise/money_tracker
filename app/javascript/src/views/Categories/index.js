import React from 'react'
import { Table, Icon, Row, Col, Button } from 'antd'
import styled from 'styled-components'
import { observer, inject } from 'mobx-react'
import { observable } from 'mobx'

import CategoryForm from './components/CategoryForm'
import CategoryActions from '../components/InlineActions'

const White = styled.div`
  background: #fff;
`
const PullRight = styled.span`
  float: right;
`

@inject('categories', 'formStore')
@observer
class Categories extends React.Component {
  constructor(props){
    super(props)
    this.state = { showForm: null }

    window.categories = props.categories
  }

  columns = [{
    title: '',
    dataIndex: 'icon',
    render: iconType => <Icon type={iconType || 'folder'} />
  }, {
    title: 'Name',
    dataIndex: 'name',
  }, {
    title: 'Budget',
    dataIndex: 'budget',
  }, {
    title: 'Actions',
    dataIndex: 'id',
    render: id => <CategoryActions resourceId={id}
                                   editResource={this.showEditForm.bind(this, id)}
                                   deleteResource={this.props.categories.delete} />
  }]

  showNewForm(){
    this.props.formStore.reset()
    this.setState({showForm: 'new'})
  }

  showEditForm(categoryId){
    var category = this.props.categories.list.find(c => c.id === categoryId )
    this.props.formStore.load(category)
    this.setState({ showForm: 'edit' })
  }

  hideForm(){
    this.setState({ showForm: null })
    this.props.formStore.reset()
  }

  render(){
    const { list, status, create, update } = this.props.categories
    return(
      <div>
        <CategoryForm onComplete={this.hideForm.bind(this)}
                      action={create}
                      visible={this.state.showForm === 'new'}/>
        <CategoryForm onComplete={this.hideForm.bind(this)}
                      action={update}
                      visible={this.state.showForm === 'edit'}/>
        <Row>
          <Col>
            <PullRight>
              <Button type="primary" onClick={this.showNewForm.bind(this)}>
                <Icon type="plus" />
                Create category
              </Button>
            </PullRight>
          </Col>
        </Row>
        <br />
        <Row>
          <Col span={24}>
            <White>
              <Table columns={this.columns} dataSource={list}
                     pagination={false} loading={status.isLoading}/>
            </White>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Categories