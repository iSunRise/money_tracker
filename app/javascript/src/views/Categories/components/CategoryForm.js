import React from 'react'
import { Modal, Icon, Form, Input, Button, Select } from 'antd'
import { observer, inject } from 'mobx-react'

const Option = Select.Option

@inject('formStore', 'budgets')
@observer
class CategoryForm extends React.Component {
  constructor(props){
    super(props)

    this.onSave = this.onSave.bind(this)
    this.onChange = this.onChange.bind(this)
    this.onBudgetIdChange = this.onBudgetIdChange.bind(this)
  }

  onChange(ev){
    this.props.formStore.setField(ev.target.name, ev.target.value)
  }

  onBudgetIdChange(budgetId){
    this.props.formStore.setField('budgetId', budgetId)
  }

  onSave(){
    this.props.action(
      this.props.formStore.data.toJS(), this.props.onComplete,
      (errors) => {
        alert(errors[0])
      }
    )
  }

  render(){
    const fields = this.props.formStore.data.toJS()
    const budgets = this.props.budgets.list

    return(
      <Modal title="New category"
             visible={this.props.visible}
             onOk={this.onSave}
             okText='Save'
             onCancel={this.props.onComplete}>
        <Form>
          <Form.Item label='Name'>
            <Input prefix={<Icon type='bars' />}
                   placeholder='Name'
                   onChange={this.onChange} name='name'
                   value={fields.name}/>
          </Form.Item>
          <Form.Item label='Default budget'>
            <Select onChange={this.onBudgetIdChange}
                    value={fields.budgetId ? String(fields.budgetId) : '0'}
                    name='budgetId'
                    style={{ width: '100%' }}>
              <Option key={'0'} disabled={true}>Please select budget</Option>
              {budgets.map((b) => <Option key={b.id}>{b.name}</Option>)}
            </Select>
          </Form.Item>
        </Form>
      </Modal>
    )
  }
}

export default CategoryForm