import React from 'react'
import { Table, Icon, Row, Col, Button } from 'antd'
import styled from 'styled-components'
import { observer, inject } from 'mobx-react'
import { observable } from 'mobx'
import moment from 'moment'

import BudgetForm from './components/BudgetForm'
import MonthsMenu from '../components/MonthsMenu'
import BudgetActions from '../components/InlineActions'

const White = styled.div`
  background: #fff;
`
const PullRight = styled.span`
  float: right;
`

@inject('budgets', 'formStore', 'currencyStore')
@observer
class Budgets extends React.Component {
  constructor(props){
    super(props)
    this.state = { showForm: null }
  }

  columns = [{
    title: '',
    dataIndex: 'icon',
    render: iconType => <Icon type={iconType || 'folder'} />
  }, {
    title: 'Name',
    dataIndex: 'name',
  }, {
    title: 'Amount',
    dataIndex: 'amount',
    render: (amount, record, index) => `${record.currencySymbol} ${amount}`
  }, {
    title: 'Show on dashboard',
    dataIndex: 'showOnDashboard',
    render: (show) => show ? <Icon type="check-circle" /> : <Icon type="close-circle-o" />
  },{
    title: "Include in total",
    dataIndex: 'includeInTotal',
    render: (show) => show ? <Icon type="check-circle" /> : <Icon type="close-circle-o" />
  },{
    title: 'Actions',
    dataIndex: 'id',
    render: id => <BudgetActions resourceId={id}
                                 editResource={this.showEditForm.bind(this, id)}
                                 deleteResource={this.props.budgets.delete} />
  }]

  componentDidMount(){
    const date = new Date()
    this.props.budgets.load({
      params: {month: (date.getMonth() + 1), year: date.getFullYear()}
    })
  }

  showNewForm(){
    this.setState({showForm: 'new'})
  }

  showEditForm(budgetId){
    const editedBudget = this.props.budgets.list.find(c => c.id === budgetId )
    const month = this.props.formStore.getField('month')
    const year = this.props.formStore.getField('year')
    this.props.formStore.load({ ...editedBudget, month: month, year: year })
    this.setState({ showForm: 'edit' })
  }

  hideForm(){
    this.setState({showForm: null})
    const month = this.props.formStore.getField('month')
    const year = this.props.formStore.getField('year')
    this.props.formStore.load({ month: month, year: year })
  }

  onDateChange(month, year){
    this.props.formStore.setField('month', month)
    this.props.formStore.setField('year', year)
    this.props.budgets.load({month: month, year: year})
  }

  render(){
    const { data, status, editedBudget, create, update } = this.props.budgets
    const list = data.toJS()

    return(
      <div>
        <BudgetForm onComplete={this.hideForm.bind(this)}
                    action={create}
                    visible={this.state.showForm === 'new'}/>
        <BudgetForm onComplete={this.hideForm.bind(this)}
                    action={update}
                    visible={this.state.showForm === 'edit'}/>
        <Row>
          <Col>
            <PullRight>
              <Button type="primary" onClick={this.showNewForm.bind(this)}>
                <Icon type="plus" />
                Add budget
              </Button>
            </PullRight>
          </Col>
        </Row>
        <br />
        <MonthsMenu onDateChange={this.onDateChange.bind(this)}/>
        <Row>
          <Col span={24}>
            <White>
              <Table columns={this.columns} dataSource={list}
                     pagination={false} loading={status.isLoading}/>
            </White>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Budgets