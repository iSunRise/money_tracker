import React from 'react'
import moment from 'moment'
import { Modal, Icon, Form, Input, Button, DatePicker, Checkbox, Select, Row, Col } from 'antd'
import { observer, inject } from 'mobx-react'

const MonthPicker  = DatePicker.MonthPicker
const Option = Select.Option

@inject('formStore', 'currencyStore')
@observer
class BudgetForm extends React.Component {

  onChange(ev){
    this.props.formStore.setField(ev.target.name, ev.target.value)
  }

  onCheck(ev){
    this.props.formStore.setField(ev.target.name, ev.target.checked)
  }

  dateChanged(_moment, dateString){
    const [year, month] = dateString.split('-')
    this.props.formStore.setField('month', month)
    this.props.formStore.setField('year', year)
    this.props.formStore.setField('date', new Date(year, month -1, 1))
  }

  onSave(){
    this.props.action(
      this.props.formStore.data.toJS(), this.props.onComplete,
      (errors) => {
        alert(errors[0])
      }
    )
  }

  onCurrencyChange(symbol){
    this.props.formStore.setField('currencySymbol', symbol)
  }

  render(){
    const fields = this.props.formStore.data.toJS()
    const date = (fields.month && fields.year) ? new Date(fields.year, fields.month - 1, 1) : new Date()

    const currencies = this.props.currencyStore.currencies

    return(
      <Modal title="Add budget"
             visible={this.props.visible}
             onOk={this.onSave.bind(this)}
             okText="Save"
             onCancel={this.props.onComplete}>
        <Form >
          <Form.Item label="Name">
            <Input prefix={<Icon type="bars" />}
                   placeholder="Name"
                   onChange={this.onChange.bind(this)} name="name"
                   value={fields.name}/>
          </Form.Item>
          <Row gutter={4}>
            <Col md={18}>
              <Form.Item label="Amount">
                <Input prefix={<Icon type="credit-card" />}
                      placeholder="Amount"
                      onChange={this.onChange.bind(this)} name="amount"
                      value={fields.amount}/>
              </Form.Item>
            </Col>
            <Col md={6}>
              <Form.Item label="Currency">
                <Select onChange={this.onCurrencyChange.bind(this)}
                        style={{ marginTop: '1px' }}
                        value={fields.currencySymbol}>
                  {currencies.map((currency, i) => {
                    return (
                      <Option key={currency.symbol}>{currency.label}</Option>
                    )
                  })}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Form.Item label="Month">
            <MonthPicker onChange={this.dateChanged.bind(this)}
                         value={moment(date, 'YYYY-MM-DD')}
                         format="MMMM YYYY"/>


          </Form.Item>
          <Form.Item label="Settings">
            <Checkbox onChange={this.onCheck.bind(this)} name="showOnDashboard"
                      checked={fields.showOnDashboard} >
              Show on dashboard
            </Checkbox>
            <Checkbox onChange={this.onCheck.bind(this)} name="includeInTotal"
              checked={fields.includeInTotal} >
              Include in total
            </Checkbox>
          </Form.Item>
        </Form>
      </Modal>
    )
  }
}

export default BudgetForm