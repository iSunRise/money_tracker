import React from 'react'
import { Table, Icon, Button } from 'antd'
import styled from 'styled-components'
import Moment from 'react-moment'
import moment from 'moment'
import { sprintf } from 'sprintf-js'

import ExpenceActions from '../../components/InlineActions'

import './ExpencesTable.css'

const White = styled.div`
  background: #fff;
`
class ExpencesTable extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      currentDateString: new Date().toDateString()
    }
  }

  columns = [{
    title: 'Date',
    dataIndex: 'date',
    width: '14%',
    render: (date, row) => {return(
      <div className="label-add-expence">
        { row.label ? <Button shape="circle" icon="plus" size="small" onClick={this.showNewForm.bind(this, date)}/> : null }
        <Moment format="DD.MM.YYYY">{date}</Moment>
      </div>
    )}
  }, {
    title: 'Amount',
    dataIndex: 'amount',
    width: '10%',
    render: (amount, record) => {
      if (record.label){
        var amounts = []
        for (var currencySymbol in record.amounts){
          amounts.push(`${currencySymbol} ${sprintf('%.2f', record.amounts[currencySymbol])}`)
        }
        return amounts.join(', ')
      } else {
        const symbol = record.currencySymbol ? record.currencySymbol : ''
        if (record.rate && record.rate > 1){
          return `${symbol} ${sprintf('%.2f', amount)} (₴ ${sprintf('%.2f', amount * record.rate)})`
        } else {
          return `${symbol} ${sprintf('%.2f', amount)}`
        }
      }
    }
  }, {
    title: 'Name',
    dataIndex: 'name',
    width: '30%'
  }, {
    title: 'Category',
    dataIndex: 'categoryName',
    width: '20%'
  }, {
    title: 'Budget',
    dataIndex: 'budgetName',
    width: '20%'
  },{
    title: 'Actions',
    dataIndex: 'id',
    render: (id, row, index) => {
      if(row.label) {return null }
      return <ExpenceActions resourceId={id || 0}
                             editResource={this.showEditForm.bind(this, id)}
                             deleteResource={this.props.deleteExpence} />
    }
  }]

  showNewForm(date){
    this.props.showNewForm({date: moment(date).format('YYYY-MM-DD')})
  }

  showEditForm(expenceId){
    this.props.showEditForm(expenceId)
  }

  rowClassName(record, index){
    if(!record.label){ return '' }
    if(record.label && record.date.toDateString() === this.state.currentDateString){
      return 'label-row currentDay'
    } else {
      return 'label-row'
    }
  }

  render(){
    return(
      <White>
        <Table columns={this.columns}
               dataSource={this.props.data}
               pagination={false}
               locale={{emptyText: 'No expences'}}
               rowClassName={this.rowClassName.bind(this)}
               size="middle"/>
      </White>
    )
  }
}

export default ExpencesTable
