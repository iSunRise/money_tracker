import React from 'react'
import { Row, Col, Progress, Card, Icon } from 'antd'
import { observer, inject } from 'mobx-react'
import { sprintf } from 'sprintf-js'
import { Link } from 'react-router-dom'
import queryString from 'query-string'

import { White, TextCenter } from '../../utils'



@inject('expences', 'budgets', 'currencyStore', 'routing')
@observer
class BudgetsStatuses extends React.Component {

  budgetExpencesAmount(budget, expences){
    let amount = 0;
    expences.forEach((ex)=>{ 
      if(ex.budgetId == budget.id){
        amount += parseFloat(ex.amount)
      }
    })
    return amount
  }

  enrichWithExpences(budgets, expences){
    const { budgetId } = queryString.parse(this.props.routing.location.search)
    return budgets.map((b)=>{
      if(!budgetId || budgetId == 'all' || b.id.toString() == budgetId ){
        b.expencesAmount = this.budgetExpencesAmount(b, expences)
        return b
      }
    }).filter(el => el)
  }

  render(){
    const expences = this.props.expences.list
    const list = this.enrichWithExpences(this.props.budgets.list, expences)
    const rates = this.props.currencyStore.rates

    // generate "Total" box
    var totalBudgetsAmount = 0
    var totalBudgetsExpences = 0
    list.forEach(function (budget) {
      if(budget.includeInTotal){
        totalBudgetsAmount += budget.amount * (rates[budget.currencySymbol] || 1)
        totalBudgetsExpences += budget.expencesAmount * (rates[budget.currencySymbol] || 1)
      }
    })
    var displayList = [{
      id: 'all',
      amount: totalBudgetsAmount,
      name: "Total",
      showOnDashboard: true,
      currencySymbol: '₴',
      expencesAmount: totalBudgetsExpences
    }].concat(list)

    return(
      <div>
        <Row gutter={16}>
          {
            displayList.filter(b => b.showOnDashboard).map((bud, i)=>{
              let percent = Math.round(100 * (bud.amount - bud.expencesAmount) / bud.amount)
              const status = percent < 0 ? 'exception' : 'success'
              const amountLeftColor = percent < 0 ? '#f04634' : '#00a854'

              return(
                <Col key={i} span={4} style={{marginBottom: 16}}>
                  <TextCenter>
                    <Card title={<Link to={`/?budgetId=${bud.id}`}>{bud.name}</Link>} bordered={false}>
                      <White>
                        <table style={{width: '100%'}}>
                          <tbody>
                            <tr>
                              <td style={{textAlign: 'left'}}>Total:</td>
                              <td style={{textAlign: 'right'}}>
                                <span style={{color: '#0f8ee9'}}>
                                  {bud.currencySymbol} {sprintf('%.2f', bud.amount)}
                                </span>
                              </td>
                            </tr>
                            <tr>
                              <td style={{textAlign: 'left'}}>Spent:</td>
                              <td style={{textAlign: 'right'}}>
                                <span style={{color: '#f04634'}}>
                                  {sprintf('%.2f', bud.expencesAmount)}
                                </span>
                              </td>
                            </tr>
                            <tr>
                              <td style={{textAlign: 'left'}}>Left:</td>
                              <td style={{textAlign: 'right'}}>
                                <span style={{color: amountLeftColor}}>
                                  {sprintf('%.2f', (bud.amount - bud.expencesAmount))}
                                </span>
                              </td>
                            </tr>

                            <tr>
                              <td colSpan="2">
                                <Progress type="line" percent={percent < 0 ? 100 :  percent} strokeWidth={5}
                                          format={ (p) => `${percent}%` }
                                          status={status} showInfo={false}/>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </White>
                    </Card>
                  </TextCenter>
                </Col>
            )
          }) }
        </Row>
      </div>
    )
  }
}

export default BudgetsStatuses