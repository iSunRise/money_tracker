import React from 'react'
import { Modal, Icon, Form, Input, Button, Select, DatePicker, Row, Col } from 'antd'
import { observer, inject } from 'mobx-react'
import moment from 'moment'
import { sprintf } from 'sprintf-js'

const Option = Select.Option

@inject('expences', 'categories', 'budgets', 'formStore', 'currencyStore')
@observer
class ExpenceForm extends React.Component {
  constructor(props){
    super(props)

    this.onSave = this.onSave.bind(this)
    this.onChange = this.onChange.bind(this)
    this.onCategoryIdChange = this.onCategoryIdChange.bind(this)
    this.onBudgetIdChange = this.onBudgetIdChange.bind(this)
    this.dateChanged = this.dateChanged.bind(this)
  }


  onChange(ev){
    this.props.formStore.setField(ev.target.name, ev.target.value)
  }

  onCategoryIdChange(categoryId, option){
    this.props.formStore.setField('categoryId', categoryId)
    if(!this.props.formStore.getField('budgetId')){
      this.props.formStore.setField('budgetId', option.props.budgetId)
    }
  }

  dateChanged(_moment, dateString){
    this.props.formStore.setField('date', dateString)
  }

  onBudgetIdChange(budgetId){
    this.props.formStore.setField('budgetId', budgetId)
  }

  onSave(){
    const { update, create } = this.props.expences
    const payload = this.props.formStore.data.toJS()
    const action = payload.id ? update : create

    action(
      payload, this.props.onComplete,
      (errors) => {
        alert(errors[0])
      }
    )
  }

  onCurrencyChange(rates){
    return (symbol) => {
      this.props.formStore.setField('currencySymbol', symbol)
      this.props.formStore.setField('rate', rates[symbol])
    }
  }

  render(){
    const fields = this.props.formStore.data.toJS()
    const currencies = this.props.currencyStore.currencies
    const rates = this.props.currencyStore.rates

    return(
      <Modal title="New expence"
             visible={this.props.visible}
             onOk={this.onSave}
             okText='Save'
             onCancel={this.props.onComplete}>
        <Form>
          <Row>
            <Col span={4}>
              <Form.Item label='Amount'>
                <Input prefix={<Icon type='credit-card' />}
                      onChange={this.onChange} name='amount'
                      value={fields.amount}/>
              </Form.Item>
            </Col>
            <Col span={19} offset={1}>
              <Form.Item label='Name'>
                <Input prefix={<Icon type='bars' />}
                       onChange={this.onChange} name='name'
                       value={fields.name}/>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={9}>
              <Form.Item label="Date">
                <DatePicker onChange={this.dateChanged} style={{width: '100%'}}
                            value={moment(fields.date, 'YYYY-MM-DD')}/>
              </Form.Item>
            </Col>
            <Col span={9} offset={1}>
              <Form.Item label="Currency">
                <Select onChange={this.onCurrencyChange(rates)}
                  style={{ marginTop: '1px' }}
                  value={fields.currencySymbol}>
                  {currencies.map((currency, i) => {
                    return (
                      <Option key={currency.symbol}>{currency.label}</Option>
                    )
                  })}
                </Select>
              </Form.Item>
            </Col>
            <Col span={4} offset={1}>
              <Form.Item label="Rate">
                <Input value={sprintf('%.2f', rates[fields.currencySymbol] || 1)}
                       onChange={this.onChange} name='rate'/>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={11}>
              <Form.Item label='Category'>
                <Select onSelect={this.onCategoryIdChange}
                        value={fields.categoryId ? String(fields.categoryId) : '0'}
                        name='categoryId'>
                  <Option key={'0'} disabled={true}>Please select category</Option>
                  {this.props.categories.list.map((c) =>
                    <Option key={c.id} budgetId={c.budgetId} >{c.name}</Option>)
                  }
                </Select>
              </Form.Item>
            </Col>
            <Col span={12} offset={1}>
              <Form.Item label='Budget'>
                <Select onChange={this.onBudgetIdChange}
                        value={fields.budgetId ? String(fields.budgetId) : '0'}
                        name='budgetId'>
                  <Option key={'0'} disabled={true}>Please select budget</Option>
                  {this.props.budgets.list.map((b) => <Option key={b.id}>{b.name}</Option>)}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Form.Item label='Description'>
            <Input.TextArea prefix={<Icon type='file-text' />}
                            onChange={this.onChange} name='description'
                            value={fields.description}/>
          </Form.Item>
        </Form>
      </Modal>
    )
  }
}

export default ExpenceForm