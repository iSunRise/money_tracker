import React from 'react'
import { Row, Col, Button, Icon } from 'antd'
import Moment from 'react-moment'
import moment from 'moment'
import styled from 'styled-components'
import { observer, inject } from 'mobx-react'
import { observable } from 'mobx'
import queryString from 'query-string'

const PullRight = styled.span`
  float: right;
`

import BudgetsStatuses from './components/BudgetsStatuses'
import ExpencesTable from './components/ExpencesTable'
import ExpenceForm from './components/ExpenceForm'
import MonthsMenu from '../components/MonthsMenu'

const HeadRow = styled.div`
  width: 100%;
  background: rgb(247, 247, 247);
  color: rgba(0, 0, 0, 0.85);
  padding: 5px;
  border-left: 1px solid #fff;
  border-right: 1px solid #fff;
`

@inject('expences', 'formStore', 'routing')
@observer
class Dashboard extends React.Component {
  constructor(props){
    super(props)
    this.state = { showForm: false }
  }

  componentWillReceiveProps(){
    const params = queryString.parse(this.props.routing.location.search)
    if(params.budgetId){
      this.props.expences.load({budgetId: params.budgetId})
    }
  }

  showEditForm(expenceId){
    const expence = this.props.expences.list.find(e => e.id === expenceId)
    expence.date = moment(expence.date).format('YYYY-MM-DD')
    this.props.formStore.load(expence)
    this.setState({ showForm: true })
  }

  showNewForm(initialState = {}){
    if(!initialState.date){
      initialState['date'] = moment(new Date()).format('YYYY-MM-DD')
    }
    this.setState({ showForm: true })
    this.props.formStore.load(initialState)
  }

  hideForm(){
    this.props.formStore.reset()
    this.setState({ showForm: null })
  }

  dateChanged(month, year){
    this.props.expences.loadExpences(month, year)
  }


  render(){
    console.log()
    const { expencesArray, status } = this.props.expences

    return(
      <div>
        <ExpenceForm onComplete={this.hideForm.bind(this)}
                     visible={this.state.showForm}/>
        <Row>
          <Col>
            <PullRight>
              <Button type="primary" onClick={this.showNewForm.bind(this, {})}>
                <Icon type="plus" />
                Add expence
              </Button>
            </PullRight>
          </Col>
        </Row>
        <br />
        <Row>
          <Col>
            <BudgetsStatuses/>
          </Col>
        </Row>
        <MonthsMenu onDateChange={this.dateChanged.bind(this)}/>
        <ExpencesTable data={expencesArray}
                       showHeader={true} loading={status.isLoading}
                       showNewForm={this.showNewForm.bind(this)}
                       showEditForm={this.showEditForm.bind(this)}
                       deleteExpence={this.props.expences.delete}/>
      </div>
    )
  }
}

export default Dashboard