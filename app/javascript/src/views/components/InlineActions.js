import React from 'react'
import { Popconfirm, Button, Icon } from 'antd'
import PropTypes from 'prop-types'

const InlineActions = (props) => {
  function confirmDelete(){
    props.deleteResource(props.resourceId, ()=>{}, ()=>{})
  }

  return(
    <div>
      <Button onClick={props.editResource}>
        <Icon type="edit" />
      </Button>
      <span>&nbsp;</span>
      <Popconfirm title="Are you sure delete this?"
                  onConfirm={confirmDelete}
                  okText="Yes" cancelText="No">
        <Button type="danger">
          <Icon type="delete" />
        </Button>
      </Popconfirm>
    </div>
  )
}

InlineActions.propTypes = {
  resourceId: PropTypes.number.isRequired,
  editResource: PropTypes.func.isRequired,
  deleteResource: PropTypes.func.isRequired,
}

export default InlineActions