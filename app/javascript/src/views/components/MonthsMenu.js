import React from 'react'
import { Menu, Icon, DatePicker, Row, Col } from 'antd'

import { White, PullRight } from '../utils'

const MonthPicker = DatePicker.MonthPicker

class MonthsMenu extends React.Component {
  constructor(props){
    super(props)

    const date = new Date()
    this.state = {
      currentMonth: date.getMonth() + 1,
      selectedMonth: date.getMonth() + 1,
      currentYear: date.getFullYear()
    }
  }

  months = {
    1: 'January',
    2: 'February',
    3: 'March',
    4: 'April',
    5: 'May',
    6: 'June',
    7: 'July',
    8: 'August',
    9: 'September',
    10: 'October',
    11: 'November',
    12: 'December'
  }


  changeSelectedMonth(menuItem){
    const monthNumber = Number(menuItem.key)
    this.setState({selectedMonth: monthNumber})
    this.props.onDateChange(
      this.normalizeMonthNumber(monthNumber), this.monthYear(monthNumber)
    )
  }

  changeDate(momentDate){
    const monthNumber = momentDate.month() + 1
    this.setState({
      currentMonth: monthNumber,
      selectedMonth: monthNumber,
      currentYear: momentDate.year()
    })
    this.props.onDateChange(
      monthNumber, this.monthYear(monthNumber)
    )
  }

  monthTitle(number){
    return this.months[this.normalizeMonthNumber(number)]
  }

  monthYear(number){
    if(number > 12){ return this.state.currentYear + 1 }
    if(number < 1){ return this.state.currentYear - 1 }
    return this.state.currentYear
  }

  normalizeMonthNumber(number){
    if(number > 12){ number -= 12 }
    if(number < 1){ number += 12 }
    return number
  }

  render(){
    return(
      <White>
        <Row>
          <Col span={20}>
            <Menu
              onClick={this.changeSelectedMonth.bind(this)}
              mode="horizontal"
              selectedKeys={[String(this.state.selectedMonth)]}>
              { [1, 2, 3, 4, 5].map((n) => {
                const monthNumber = this.state.currentMonth + n - 3
                return(
                  <Menu.Item key={String(monthNumber)} >
                    <Icon type="calendar" />
                    { this.monthTitle(monthNumber) }
                    &nbsp;
                    { this.monthYear(monthNumber) }
                  </Menu.Item>
                )
              })}
            </Menu>
          </Col>
          <Col span={4}>
            <PullRight style={{padding: '10px'}}>
              <MonthPicker onChange={this.changeDate.bind(this)}/>
            </PullRight>
          </Col>
        </Row>
      </White>
    )
  }
}

export default MonthsMenu