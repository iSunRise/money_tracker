import { observable, action } from 'mobx'
import Client from '../api/Client'

class CurrencyStore {
  constructor(){
    this.rates = observable({})
    this.loadRates()
  }

  currencies = [{
    symbol: '₴',
    nbuId: null,
    label: '₴ - грн.'
  }, {
    symbol: '$',
    nbuId: 840,
    label: '$ - USD.'
  }, {
    symbol: '€',
    nbuId: 978,
    label: '€ - EUR.'
  }]

  @action.bound loadRates(){
    Client.execute('get', 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json',
                   {}, null, this.onRatesLoadSucess.bind(this), this.onRatesLoadError)
  }

  onRatesLoadSucess(response){
    var rates = {}
    this.currencies.forEach((currency)=>{
      var nbuCurrency = response.find(el => el['r030'] === currency.nbuId)
      if(nbuCurrency){
        rates[currency.symbol] = nbuCurrency.rate
      }
    })
    this.rates = rates
  }

  onRatesLoadError(error) {
    console.error('Cannot load currencies from bank.gov.ua')
    console.error(error)
  }
}

export default CurrencyStore

