import CategoryResource from './categories'
import BudgetResource from './budgets'
import ExpenceResource from './expences'
import FormStore from './form'
import CurrencyStore from './currencies'
import StatisticResource from './statistics'

export default {
  categories: new CategoryResource(),
  budgets: new BudgetResource(),
  expences: new ExpenceResource(),
  formStore: new FormStore(),
  currencyStore: new CurrencyStore(),
  statistic: new StatisticResource()
}