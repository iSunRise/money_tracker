import { computed, observable, action } from 'mobx'
import BaseResource from './resource'
import { ExpenceApi } from '../api'


class ExpenceResource extends BaseResource {
  constructor(props){
    super(props, 'ExpenceChannel')
    this.api = ExpenceApi

    const date = new Date()
    this.month = date.getMonth()
    this.year = date.getFullYear()
  }

  @computed get expencesArray(){
    const daysInMonthNumber = new Date(this.year, this.month + 1, 0).getDate()
    const expences = {}
    // build header rows for each calendar day
    for(var i = 1; i <= daysInMonthNumber; i++){
      expences[i] = [{
        date: new Date(this.year, this.month, i),
        label: true,
        key: `label-${i}`,
        amounts: {}
      }]
    }
    // fullfill days with expences
    this.list.forEach((exp) => {
      if (!expences[exp.dayNumber][0].amounts[exp.currencySymbol]){
        expences[exp.dayNumber][0].amounts[exp.currencySymbol] = 0
      }
      expences[exp.dayNumber][0].amounts[exp.currencySymbol] += Number(exp.amount)
      expences[exp.dayNumber].push(exp)
    })
    // prepare array
    var expencesArray = []
    for(var i = 1; i <= daysInMonthNumber; i++){
      expencesArray = expencesArray.concat(expences[i])
    }
    return expencesArray
  }

  @action loadExpences(month, year){
    const daysInMonthNumber = new Date(year, month, 0).getDate()
    const fromDate = `01.${month}.${year}`
    const toDate = `${daysInMonthNumber}.${month}.${year}`
    this.month = month - 1 // weird js months numbering
    this.year = year
    this.load({ from: fromDate, to: toDate })
  }

}

export default ExpenceResource
