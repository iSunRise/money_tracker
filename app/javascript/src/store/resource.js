import { observable, computed, action } from 'mobx'

class BaseResource {
  @observable status
  @observable data

  constructor(props, channelName){
    this.status = observable.object({
      isLoading: false,
      loaded: false
    })
    this.data = observable.shallowArray([])
    // this.api = ... <- must be set in child class
    // websockets (action cable)
    if(channelName){
      this.subscribeOn(channelName)
    }
  }

  subscribeOn(channelName){
    App.cable.subscriptions.create({
      channel: channelName
    }, {
      received: this.remoteUpdateReceived.bind(this)
    })
  }

  @action remoteUpdateReceived(payload){
    if(!this.status.loaded){ return } // skip if we don't operate with this resouce yet
    const record = this.data.find(rec => rec.id === payload.record.id)

    switch(payload.event){
      case 'record/@created': {
        if(record){ return } // skip if record already exists
        this.data.unshift(payload.record)
        break
      }
      case 'record/@updated': {
        if(!record){ return } // skip if we don't have such record
        const index = this.data.indexOf(record)
        this.data[index] = payload.record
        break
      }
      case 'record/@deleted': {
        if(!record){ return } // skip if we don't have such record
        this.data.remove(record)
        break
      }
    }
  }

  @computed get list(){
    if(this.status.loaded){
      return this.data.toJS()
    }
    this.load()
    return []
  }

  @action.bound load(payload = {}){
    this.status.isLoading = true
    this.api.load(
      payload,
      (data) => {
        this.data.replace(data)
        this.status.isLoading = false
        this.status.loaded = true
      },
      (errors) => {
        console.error(errors)
        this.status.isLoading = false
      }
    )
  }

  @action.bound create(params, onSuccess, onError){
    this.status.isLoading = true
    this.api.create(
      params,
      (resource) => {
        // record might be already added via websockets
        const existingRecord = this.data.find(r => r.id === resource.id)
        if(!existingRecord){
          this.data.unshift(resource)
        }
        this.status.isLoading = false
        onSuccess(resource)
      },
      (errors) => {
        console.error(errors)
        this.status.isLoading = false
        onError(errors)
      }
    )
  }

  @action.bound update(params, onSuccess, onError){
    this.status.isLoading = true
    this.api.update(
      params,
      (resource) => {
        this.status.isLoading = false
        var cat = this.data.find((c) => c.id === params.id)
        var index = this.data.indexOf(cat)
        this.data[index] = resource
        onSuccess(resource)
      },
      (errors) => {
        console.error(errors)
        this.status.isLoading = false
        onError(errors)
      }
    )
  }

  @action.bound delete(id, onSuccess, onError){
    this.status.isLoading = true
    this.api.delete(
      id,
      () => {
        var cat = this.data.find((c) => c.id === id)
        if(cat){
          const index = this.data.indexOf(cat)
          this.data.splice(index, 1)
        }
        this.status.isLoading = false
        onSuccess()
      },
      (errors) => {
        console.error(errors)
        this.status.isLoading = false
        onError(errors)
      }
    )
  }
}

export default BaseResource