import { observable, computed, action } from 'mobx'

import BaseResource from './resource'
import { StatisticApi } from '../api'


class StatisticResource extends BaseResource {
  constructor(props) {
    super(props)
    this.api = StatisticApi
  }

  @action.bound load(payload = {}){
    this.status.isLoading = true
    this.api.load(
      payload,
      (data) => {
        this.data.replace(data)
        this.status.isLoading = false
        this.status.loaded = true
      },
      (errors) => {
        console.error(errors)
        this.status.isLoading = false
      }
    )
  }
}

export default StatisticResource
