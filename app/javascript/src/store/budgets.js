import { observable, computed, action } from 'mobx'

import BaseResource from './resource'
import { BudgetApi } from '../api'


class BudgetResource extends BaseResource {
  constructor(props){
    super(props, 'BudgetChannel')
    this.api = BudgetApi
  }
}

export default BudgetResource
