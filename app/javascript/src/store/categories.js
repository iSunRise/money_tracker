import BaseResource from './resource'
import { CategoryApi } from '../api'


class CategoryResource extends BaseResource {
  constructor(props){
    super(props, 'CategoryChannel')
    this.api = CategoryApi
  }
}

export default CategoryResource