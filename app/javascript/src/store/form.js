import { observable, action } from 'mobx'

class FormStore {
  @observable data

  constructor(){
    this.data = observable.map({})
  }

  @action.bound getField(name){
    return this.data.get(name)
  }

  @action.bound setField(name, value){
    this.data.set(name, value)
  }

  @action.bound load(newData = {}){
    this.data.replace(newData)
  }

  @action.bound reset(){
    this.data.clear()
  }
}


export default FormStore