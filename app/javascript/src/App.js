import React from 'react'
import { Route, Link } from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'
import { Provider } from 'mobx-react'
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router'
import { Router } from 'react-router'

// layout
import { Layout, Menu, Icon } from 'antd'
const { SubMenu } = Menu
const { Header, Content, Sider, Footer } = Layout
// localization
import { LocaleProvider } from 'antd'
import enUS from 'antd/lib/locale-provider/en_US'

const AppWithProvider = () => {
  return(
    <LocaleProvider locale={enUS}><App /></LocaleProvider>
  )
}

// routing and stores
import storesList from './store'
var routingStore = new RouterStore()
const stores = {
  ...storesList,
  routing: routingStore
}
const browserHistory = createBrowserHistory()
const history = syncHistoryWithStore(browserHistory, routingStore)

import './App.less'

import Navbar from './components/Navbar'
// import Breadcrumbs from './components/Breadcrumbs'
import Dashboard from './views/Dashboard'
import Categories from './views/Categories'
import Budgets from './views/Budgets'
import Statistics from './views/Statistics'


class App extends React.Component {
  render(){
    return(
      <LocaleProvider locale={enUS}>
        <Provider {...stores}>
          <Router history={history}>
            <Layout className="layout">
              <Navbar />
              <Content style={{ padding: '16px 50px' }}>
                <div style={{ minHeight: 280 }}>
                  <Route exact path={'/'} component={Dashboard}/>
                  <Route exact path={'/categories'} component={Categories}/>
                  <Route exact path={'/budgets'} component={Budgets}/>
                  <Route exact path={'/statistic'} component={Statistics}/>
                  {/* <Route path={'/profile'} component={Profile}/> */}
                  {/* <Route path={'/tasks/'} component={TaskRoutes}/> */}
                </div>
              </Content>
              <Footer style={{ textAlign: 'center' }}>
                 ©2017 Alexander
              </Footer>
            </Layout>
          </Router>
        </Provider>
      </LocaleProvider>
    )
  }
}

export default App
