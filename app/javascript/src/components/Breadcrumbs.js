import React from 'react'
import { Breadcrumb } from 'antd'
import { Link } from 'react-router-dom'


const Breadcrumbs = (props) => {
  return(
    <Breadcrumb style={{ margin: '12px 0' }}>
      <Breadcrumb.Item key={1}>
        <Link to="#">Home</Link>
      </Breadcrumb.Item>
    </Breadcrumb>
  )
}

export default Breadcrumbs