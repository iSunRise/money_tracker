import React from 'react'
import { inject, observer } from 'mobx-react'

import { Layout, Menu, Icon, Dropdown } from 'antd'

import { Link } from 'react-router-dom'
const { Item } = Menu

const menu = (
  <Menu>
    <Menu.Item>
      <Link to="/login">Login</Link>
    </Menu.Item>
  </Menu>
)

@inject('routing')
@observer
class Navbar extends React.Component {

  rules = [
    ['2', /\/categories/],
    ['3', /\/budgets/],
    ['4', /\/statistic/],
    ['6', /\/login/]
  ]

  selectedKeys(){
    var pathname = this.props.routing.location.pathname
    if(pathname === '/'){ return ['1'] }

    return this.rules.map(([key, regexp]) => {
      if(regexp.test(pathname)){ return key }
    })
  }

  render(){
    return(
      <Layout.Header>
        <div className="logo" />
        <Menu
          theme="dark"
          mode="horizontal"
          style={{ lineHeight: '64px' }}
          selectedKeys={this.selectedKeys()}
        >
          <Item key="1">
            <Link to="/">Dashboard</Link>
          </Item>
          <Item key="2">
            <Link to="/categories">Categories</Link>
          </Item>
          <Item key="3">
            <Link to="/budgets">Budgets</Link>
          </Item>
          <Item key="4">
            <Link to="/statistic">Statistic</Link>
          </Item>
        </Menu>
      </Layout.Header>
    )
  }
}


export default Navbar