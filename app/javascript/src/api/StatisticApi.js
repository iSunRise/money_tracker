import Client from './Client'

const StatisticAPI = {
  load(params = {}, onSuccess, onError) {
    Client.execute('get', '/api/statistic', { params: params }, null, onSuccess, onError)
  }
}

export default StatisticAPI