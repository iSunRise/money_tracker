import axios from 'axios'

const Client = {
  execute(method, url, payload, resourceKey, onSuccess, onError){
    axios[method](url, payload)
      .then(function (response) {
        if(resourceKey){
          var resource = response.data[resourceKey]
        } else {
          var resource = response.data
        }
        onSuccess(resource)
      })
      .catch(function (error) {
        if (error.response && error.response.code === 401) {
          alert('You need to sign in, please reload page or open new tab')
        }
        try {
          var errors = error.response.data.errors
        } catch(e) { }
        onError(errors || error)
      })
  }
}

export default Client