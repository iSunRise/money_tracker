import Client from './Client'

const BudgetApi =  {
  load(params = {}, onSuccess, onError){
    Client.execute('get', '/api/budgets', { params: params }, 'budgets', onSuccess, onError)
  },

  create(params, onSuccess, onError){
    Client.execute('post', '/api/budgets', { budget: params }, 'budget', onSuccess, onError)
  },

  update(params, onSuccess, onError){
    Client.execute('patch', `/api/budgets/${params.id}`, { budget: params }, 'budget', onSuccess, onError)
  },

  delete(id, onSuccess, onError){
    Client.execute('delete', `/api/budgets/${id}`, {}, undefined, onSuccess, onError)
  }
}

export default BudgetApi