import Client from './Client'

const ExpenceApi =  {
  load(params = {}, onSuccess, onError){
    Client.execute('get', '/api/expences', { params: params }, 'expences', onSuccess, onError)
  },

  create(params, onSuccess, onError){
    Client.execute('post', '/api/expences', { expence: params }, 'expence', onSuccess, onError)
  },

  update(params, onSuccess, onError){
    Client.execute('patch', `/api/expences/${params.id}`, { expence: params }, 'expence', onSuccess, onError)
  },

  delete(id, onSuccess, onError){
    Client.execute('delete', `/api/expences/${id}`, {}, undefined, onSuccess, onError)
  }
}

export default ExpenceApi