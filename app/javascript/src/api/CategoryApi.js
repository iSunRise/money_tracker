import Client from './Client'

const CategoryApi =  {
  load(params = {}, onSuccess, onError){
    Client.execute('get', '/api/categories', params, 'categories', onSuccess, onError)
  },

  create(params, onSuccess, onError){
    Client.execute('post', '/api/categories', { category: params }, 'category', onSuccess, onError)
  },

  update(params, onSuccess, onError){
    Client.execute('patch', `/api/categories/${params.id}`, { category: params }, 'category', onSuccess, onError)
  },

  delete(id, onSuccess, onError){
    Client.execute('delete', `/api/categories/${id}`, {}, undefined, onSuccess, onError)
  }
}

export default CategoryApi