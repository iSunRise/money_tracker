module AutoPushable
  extend ActiveSupport::Concern

  included do
    after_commit :on_record_created, on: :create
    after_commit :on_record_updated, on: :update
    after_destroy :on_record_deleted
  end


  private

  def on_record_created
    broadcast_self('record/@created')
  end

  def on_record_updated
    broadcast_self('record/@updated')
  end

  def on_record_deleted
    ActionCable.server.broadcast "Account##{self.account_id}/#{self.class.name}Channel",
                                 event: 'record/@deleted',
                                 record: { id: id }
  end

  def broadcast_self(event_type)
    serializer = "#{self.class.name}Serializer".constantize
    record = serializer.new(self).as_json.map{ |k, v|  [k.to_s.camelize(:lower), v] }.to_h
    ActionCable.server.broadcast "Account##{self.account_id}/#{self.class.name}Channel",
                                 event: event_type,
                                 record: record
  end


end