class Expence < ApplicationRecord
  include AutoPushable

  validates :date, :amount, :name, :budget, presence: true
  validates :amount, numericality: true

  belongs_to :category, optional: true
  belongs_to :budget

  delegate :name, to: :budget, prefix: true

  def category_name
    category.name if category_id.present?
  end
end
