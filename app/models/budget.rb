class Budget < ApplicationRecord
  include AutoPushable

  before_save :update_default_amount
  before_save :set_month_year
  after_save :update_monthly_budget
  after_destroy :clean_categories

  attr_accessor :amount, :month, :year

  validates :amount, :name, presence: true

  has_many :monthly_budgets, dependent: :destroy
  has_many :expences, dependent: :destroy

  def current_monthly_budget
    @_cmb ||= monthly_budgets.find_by(year: year, month: month)
  end

  private

  def update_default_amount
    return if current_monthly_budget.present?
    self.default_amount = amount
  end

  def set_month_year
    date = Date.current
    self.month ||= date.month
    self.year  ||= date.year
  end

  def update_monthly_budget
    mb = current_monthly_budget || MonthlyBudget.new(year: year, month: month, budget_id: id)
    mb.amount = amount
    mb.save!
  end

  def clean_categories
    Category.where(budget_id: id).update_all(budget_id: nil)
  end
end
