class Category < ApplicationRecord
  include AutoPushable
  validates :name, presence: true

  belongs_to :budget, optional: true
end
