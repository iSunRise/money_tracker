class Account < ApplicationRecord
  has_many :users
  has_many :expences
  has_many :categories
  has_many :budgets
end
