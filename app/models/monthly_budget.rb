class MonthlyBudget < ApplicationRecord
  validates :month, numericality: { greater_than: 0, less_than: 13 }
  validates :year, numericality: { greater_than: 2016, less_than: Date.today.year + 2 }
end
