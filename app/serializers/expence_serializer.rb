class ExpenceSerializer < ApplicationSerializer
  attributes :key, :id, :name, :amount, :date, :description, :day_number,
             :category_id, :budget_id,
             :category_name, :budget_name,
             :currency_symbol, :rate


  def day_number
    object.date.day
  end

  def currency_symbol
    object.currency_symbol || object.budget.currency_symbol || '₴'
  end
end
