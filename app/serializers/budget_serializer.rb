class BudgetSerializer < ApplicationSerializer
  attributes :key, :id, :name, :icon, :amount, :month, :year,
             :show_on_dashboard, :currency_symbol, :include_in_total
             # :expences_amount

  def amount
    month_budget.amount || object.default_amount
  end

  def month
    month_budget.month || Date.current.month
  end

  def year
    month_budget.year || Date.current.year
  end

  def expences_amount
    date = Date.new(year, month)
    object.expences.where(date: (date..date.end_of_month)).sum(:amount).to_f
  end


  def currency_symbol
    object.currency_symbol || '₴'
  end

  private

  def month_budget
    @month_budget ||= (object.current_monthly_budget || MonthlyBudget.new)
  end
end
