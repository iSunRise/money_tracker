class ApplicationSerializer < ActiveModel::Serializer
  attributes :key

  def key
    object.id
  end
end
