class CategorySerializer < ApplicationSerializer
  attributes :key, :id, :name, :icon, :budget, :budget_id

  def budget
    object.budget ? object.budget.name : ''
  end
end
