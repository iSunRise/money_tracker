class RemoveMonthAndYearFromBudget < ActiveRecord::Migration[5.1]
  def change
    remove_column :budgets, :month, :integer
    remove_column :budgets, :year, :integer
    rename_column :budgets, :amount, :default_amount
  end
end
