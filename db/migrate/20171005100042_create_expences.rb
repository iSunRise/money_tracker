class CreateExpences < ActiveRecord::Migration[5.1]
  def change
    create_table :expences do |t|
      t.string :name,    null: false
      t.decimal :amount, null: false
      t.date :date,      null: false, index: true
      t.string :description, limit: 500
      t.integer :category_id, null: false
      t.integer :budget_id,    null: false

      t.timestamps
    end

    add_foreign_key :expences, :categories
    add_foreign_key :expences, :budgets
  end
end
