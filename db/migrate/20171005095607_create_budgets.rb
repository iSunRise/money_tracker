class CreateBudgets < ActiveRecord::Migration[5.1]
  def change
    create_table :budgets do |t|
      t.integer :month,  null: false
      t.integer :year,   null: false
      t.integer :amount, null: false
      t.string :name,    null: false
      t.string :icon

      t.timestamps
    end
  end
end
