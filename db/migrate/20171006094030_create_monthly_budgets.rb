class CreateMonthlyBudgets < ActiveRecord::Migration[5.1]
  def change
    create_table :monthly_budgets do |t|
      t.integer :budget_id, null: false, index: true
      t.integer :amount, null: false
      t.integer :month, null: false, index: true
      t.integer :year, null: false, index: true

      t.timestamps
    end

    add_foreign_key :monthly_budgets, :budgets
  end
end
