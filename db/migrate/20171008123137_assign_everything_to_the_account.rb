class AssignEverythingToTheAccount < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :account_id, :integer
    add_column :expences, :account_id, :integer
    add_column :categories, :account_id, :integer
    add_column :budgets, :account_id, :integer

    account = Account.first || Account.create!
    [User, Expence, Category, Budget].each do |m_class|
      m_class.update_all(account_id: account.id)
    end

    change_column :users, :account_id, :integer, null: false
    change_column :expences, :account_id, :integer, null: false
    change_column :categories, :account_id, :integer, null: false
    change_column :budgets, :account_id, :integer, null: false

    add_index :users, :account_id
    add_index :expences, :account_id
    add_index :categories, :account_id
    add_index :budgets, :account_id

    add_foreign_key :users, :accounts
    add_foreign_key :expences, :accounts
    add_foreign_key :categories, :accounts
    add_foreign_key :budgets, :accounts
  end
end
