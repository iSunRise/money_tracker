class AddShowOnDashboardToBudgets < ActiveRecord::Migration[5.1]
  def change
    add_column :budgets, :show_on_dashboard, :boolean, default: false
  end
end
