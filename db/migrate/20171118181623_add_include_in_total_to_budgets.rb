class AddIncludeInTotalToBudgets < ActiveRecord::Migration[5.1]
  def change
    add_column :budgets, :include_in_total, :boolean, default: false
  end
end
