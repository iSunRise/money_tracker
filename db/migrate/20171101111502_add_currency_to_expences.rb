class AddCurrencyToExpences < ActiveRecord::Migration[5.1]
  def change
    add_column :expences, :currency_symbol, :string
    add_column :expences, :rate, :real, default: 1
  end
end
