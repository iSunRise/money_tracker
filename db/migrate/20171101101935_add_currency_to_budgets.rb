class AddCurrencyToBudgets < ActiveRecord::Migration[5.1]
  def change
    add_column :budgets, :currency_symbol, :string
  end
end
