class UpdateUsersToFitDeviseModel < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :encrypted_password, :string, null: false, default: ""
    ## Recoverable
    add_column :users, :reset_password_token, :string
    add_column :users, :reset_password_sent_at, :datetime

    remove_column :users, :password_digest, :string
  end
end
