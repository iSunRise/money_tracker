# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171118181623) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "budgets", force: :cascade do |t|
    t.integer "default_amount", null: false
    t.string "name", null: false
    t.string "icon"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "account_id", null: false
    t.boolean "show_on_dashboard", default: false
    t.string "currency_symbol"
    t.boolean "include_in_total", default: false
    t.index ["account_id"], name: "index_budgets_on_account_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.string "icon"
    t.integer "budget_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "account_id", null: false
    t.index ["account_id"], name: "index_categories_on_account_id"
  end

  create_table "expences", force: :cascade do |t|
    t.string "name", null: false
    t.decimal "amount", null: false
    t.date "date", null: false
    t.string "description", limit: 500
    t.integer "category_id"
    t.integer "budget_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "account_id", null: false
    t.string "currency_symbol"
    t.float "rate", default: 1.0
    t.index ["account_id"], name: "index_expences_on_account_id"
    t.index ["date"], name: "index_expences_on_date"
  end

  create_table "monthly_budgets", force: :cascade do |t|
    t.integer "budget_id", null: false
    t.integer "amount", null: false
    t.integer "month", null: false
    t.integer "year", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["budget_id"], name: "index_monthly_budgets_on_budget_id"
    t.index ["month"], name: "index_monthly_budgets_on_month"
    t.index ["year"], name: "index_monthly_budgets_on_year"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.integer "account_id", null: false
    t.index ["account_id"], name: "index_users_on_account_id"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

  add_foreign_key "budgets", "accounts"
  add_foreign_key "categories", "accounts"
  add_foreign_key "expences", "accounts"
  add_foreign_key "expences", "budgets"
  add_foreign_key "expences", "categories"
  add_foreign_key "monthly_budgets", "budgets"
  add_foreign_key "users", "accounts"
end
