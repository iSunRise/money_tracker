Rails.application.routes.draw do
  get '/', to: 'dashboard#home'

  devise_for :users

  scope :api do
    namespace :users do
      post :login, to: 'sessions#create'
    end
    resources :categories, only: [:index, :create, :update, :destroy]
    resources :budgets, only: [:index, :create, :update, :destroy]
    resources :expences, only: [:index, :create, :update, :destroy]
    resource :statistic, only: [:show]
  end

  # SPA routes
  get '/categories', to: 'dashboard#home'
  get '/budgets', to: 'dashboard#home'
  get '/statistic', to: 'dashboard#home'
  get '/login', to: 'users#login'
end
