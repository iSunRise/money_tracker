const { environment } = require('@rails/webpacker')

environment.loaders.set('js', {
  test: /\.(js|jsx)?(\.erb)?$/,
  exclude: /node_modules/,
  loader: 'babel-loader',
  options: {
    plugins: [
      ['import', { libraryName: 'antd', style: 'css' }],
      // ['import', { libraryName: 'antd', style: 'css' }],
    ],
    // This is a feature of `babel-loader` for webpack (not Babel itself).
    // It enables caching results in ./node_modules/.cache/babel-loader/
    // directory for faster rebuilds.
    cacheDirectory: true
  }
})

environment.loaders.set('css', {
  test:  /\.less$/,
  exclude: /node_modules/,
  loaders: ["style-loader", "css-loader", "less-loader"]
})

module.exports = environment
